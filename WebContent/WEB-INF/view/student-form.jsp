<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Student form</title>
</head>
<body>
	<form:form action="processForm" modelAttribute="student">
	First name: <form:input path="firstName"/><br>
	Last name: <form:input path="lastName"/><br>
	Country: <form:select path="Country">
		<form:options items="${countryOptions }"/> 
	</form:select><br>
	Fav coding language: <form:radiobuttons path="favLanguage" items="${student.favLanguageOptions }"/><br>
	Learned number systems: 2's: <form:checkbox path="numberSystems" value="2"/>
	8's: <form:checkbox path="numberSystems" value="8"/>
	10's: <form:checkbox path="numberSystems" value="10"/>
	16's: <form:checkbox path="numberSystems" value="16"/> 
	<input type="submit" value="Submit">
	</form:form>
</body>
</html>