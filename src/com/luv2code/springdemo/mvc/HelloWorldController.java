package com.luv2code.springdemo.mvc;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/hello")
public class HelloWorldController {

	@RequestMapping("/showForm")
	public String showForm(){
		return "helloworld-form";
	}
	
	@RequestMapping("/processForm")
	public String letsShoutDude(HttpServletRequest request, Model model ){
		String param = request.getParameter("studentName");
		param = param.toUpperCase();
		String result = "Yo " + param;
		model.addAttribute("message", result);
		return "helloworld";
	}
	
	@RequestMapping("/processFormTwo")
	public String processFormTwo(@RequestParam("studentName") String param, Model model ){
		param = param.toUpperCase();
		String result = "Goodday: " + param;
		model.addAttribute("message", result);
		return "helloworld";
	}
}
