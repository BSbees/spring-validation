package com.luv2code.springdemo.mvc;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class Student {

	private String firstName, lastName, country, favLanguage;
	private HashMap<String, String> favLanguageOptions;
	private Integer[] numberSystems;

	public Student() {
		favLanguageOptions = new LinkedHashMap<String, String>();
		favLanguageOptions.put("C#", "C#");
		favLanguageOptions.put("Java", "JAVA");
		favLanguageOptions.put("Ruby", "RUBY");
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getFavLanguage() {
		return favLanguage;
	}

	public void setFavLanguage(String favLanguage) {
		this.favLanguage = favLanguage;
	}

	public HashMap<String, String> getFavLanguageOptions() {
		return favLanguageOptions;
	}

	public void setFavLanguageOptions(HashMap<String, String> favLanguageOptions) {
		this.favLanguageOptions = favLanguageOptions;
	}

	public Integer[] getNumberSystems() {
		return numberSystems;
	}

	public void setNumberSystems(Integer[] numberSystems) {
		this.numberSystems = numberSystems;
	}

}
